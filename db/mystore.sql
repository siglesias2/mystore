USE myStore;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for catalogue
-- ----------------------------
DROP TABLE IF EXISTS `catalogue`;
CREATE TABLE `catalogue`  (
  `id` smallint(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
   `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  `create_by` datetime NULL DEFAULT NULL,
  `update_by` datetime NULL DEFAULT NULL,
  CONSTRAINT `pk_catalogue_id` PRIMARY KEY (`id`),
  CONSTRAINT `uq_catalogue_name` UNIQUE (`category`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- add register catalogue
-- ----------------------------
INSERT INTO catalogue(id, category) VALUES 
 (1, 'Abarrotes');
 INSERT INTO catalogue(id, category) VALUES 
 (2, 'ferreteria');
 INSERT INTO catalogue(id, category) VALUES 
 (3, 'lacteo');
 INSERT INTO catalogue(id, category) VALUES 
 (4, 'fruteria');
 INSERT INTO catalogue(id, category) VALUES 
 (5, 'hogar');
 INSERT INTO catalogue(id, category) VALUES 
 (6, 'Farmacia');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` smallint(6) NOT NULL,
  `date_delivery` date NOT NULL,
  `order_shipped` int(255) NOT NULL,
  `supplier_id` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  `create_by` datetime NULL DEFAULT NULL,
  `update_by` datetime NULL DEFAULT NULL,
  CONSTRAINT `pk_order_detail_id` PRIMARY KEY (`id`),
  CONSTRAINT `fk_order_supplier_id` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- add register for order
-- ----------------------------

INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (1, '2022-07-22', 0, 1);
 INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (2, '2022-07-23', 0, 2);
 INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (3, '2022-07-24', 0, 3);
 INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (4, '2022-07-25', 0, 4);
 INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (5, '2022-07-22', 1, 5);
 INSERT INTO order_detail(id, date_delivery, order_shipped, supplier_id) VALUES 
 (6, '2022-07-24', 0, 1);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `price_product` smallint(6) NOT NULL,
  `catalogue_id` smallint(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  `create_by` datetime NULL DEFAULT NULL,
  `update_by` datetime NULL DEFAULT NULL,
  CONSTRAINT `pk_product_id` PRIMARY KEY (`id`),
  CONSTRAINT `fk_product_catalogue` FOREIGN KEY (`catalogue_id`) REFERENCES `catalogue` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- add register products
-- ----------------------------
INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (1, 'pintura', 50, 2);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (2, 'chiles jalapeñoes 100gr', 15, 1);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (3, 'leche lala 1l', 30, 3);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (4, 'jitomate en malla', 25, 4);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (5, 'paracetamol', 20, 6);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (6, 'focos 50w', 30, 5);
 INSERT INTO product (id, name, price_product, catalogue_id) VALUES 
 (7, 'salsa valentina', 20, 1);
-- ----------------------------
-- Table structure for product_order
-- ----------------------------
DROP TABLE IF EXISTS `product_order_detail`;
CREATE TABLE `product_order_detail`  (
  `order_detail_id` smallint(6) NOT NULL,
  `product_id` smallint(6) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  `create_by` datetime NULL DEFAULT NULL,
  `update_by` datetime NULL DEFAULT NULL,
  UNIQUE INDEX `uq_product_for_order`(`order_detail_id`, `product_id`) USING BTREE,
  INDEX `fk_product_order_product`(`product_id`) USING BTREE,
  CONSTRAINT `fk_product_order_order` FOREIGN KEY (`order_detail_id`) REFERENCES `order_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_order_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- add register producto_order
-- ----------------------------
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (1, 2, 5);
 INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (1, 3, 3);
 INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (1, 4, 10);
 INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (2, 2, 13);
 INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (2, 1, 11);
 INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (2, 3, 5);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (3, 2, 12);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (3, 3, 15);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (3, 4, 3);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (4, 2, 13);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (4, 3, 20);
INSERT INTO product_order_detail(order_detail_id, product_id, quantity) VALUES 
 (4, 7, 6);

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `location` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `representative_email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `representative_phone` int NOT NULL,
 `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  `create_by` datetime NULL DEFAULT NULL,
  `update_by` datetime NULL DEFAULT NULL,
   CONSTRAINT `pk_supplier_id` PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- add register of supplier
-- ----------------------------
INSERT INTO supplier (id, name, location, representative_email, representative_phone) VALUES 
 ('1', 'valvez', 'colima', 'juan_12@valdez.com', 3123278);
 INSERT INTO supplier (id, name, location, representative_email, representative_phone) VALUES 
 ('2', 'aws', 'colima', 'pancho_12@aws.com', 3126912);
 INSERT INTO supplier (id, name, location, representative_email, representative_phone) VALUES 
 ('3', 'gitlab', 'colima', 'pedro_12@gitlab.com', 3124867);
 INSERT INTO supplier (id, name, location, representative_email, representative_phone) VALUES 
 ('4', 'surtidora ferretera', 'colima', 'javi_12@surtidoraferretera.com', 31256735);
 INSERT INTO supplier (id, name, location, representative_email, representative_phone) VALUES 
 ('5', 'avast', 'colima', 'leonel_12@valdez.com', 31223546);

SET FOREIGN_KEY_CHECKS = 1;
