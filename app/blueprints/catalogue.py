"""catalogue module"""
from hashlib import new
import json
from re import S
from flask import Blueprint, request
from datetime import datetime
from models.catalogue_model import Catalogue
from schemas.catalogue_schemas import CatalogueSchema
from response.generic_response import Responses

catalogue_bp = Blueprint('catalogue', __name__,url_prefix="/catalogues")

@catalogue_bp.get("")
def index():
    """Get all catalogue that are not deleted"""
    params = request.args
    catalogue_list = Catalogue().get_all(params)
    catalogues = CatalogueSchema(many=True).dump(catalogue_list)
    return Responses.index_response(catalogues)
@catalogue_bp.get("/<catalogue_id>")
def show(catalogue_id):
    """get an catalogue by the given id"""
    catalogue = catalogue().get_one_by({"id": catalogue_id})
    catalogue = CatalogueSchema(many=False).dump(catalogue)
    if catalogue:
        return Responses.show_response(catalogue)
    return Responses.not_found_response(catalogue)
@catalogue_bp.post("")
def create():
     """create a new catalogue"""       
     params = json.loads(request.data)
     catalogue = Catalogue(**params)
     catalogue.create()
     new_supp = catalogue.get_one_by(params)
     return Responses.create_response(CatalogueSchema(many=False).dump(new_supp))
@catalogue_bp.put("/<catalogue_id>")
def update(catalogue_id):
    """Update the catalogue according to the given id"""
    body = json.loads(request.data)
    updated_emp = Catalogue().update(catalogue_id, body)
    if updated_emp:
        catalogue = CatalogueSchema(many=False).dump(updated_emp)
        return Responses.update_response(catalogue)
    return Responses.not_found_response({"id": catalogue_id})

@catalogue_bp.delete("/<catalogue_id>")
def logic_delete(catalogue_id):
    """Make a logical delete of an catalogue"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    catalogue = Catalogue().update(catalogue_id, {"deleted_at": deleted_at})
    if catalogue:
        catalogue = CatalogueSchema(many=False).dump(catalogue)
        return Responses.logical_delete(catalogue)
    return Responses.not_found_response({"id": catalogue_id})

@catalogue_bp.delete("/<catalogue_id>/destroy")
def destroy(catalogue_id):
    """Make a permanent delete of an catalogue"""
    response = Catalogue().destroy(catalogue_id)
    if response:
        return Responses.destroy_resource()
    return Responses.not_found_response({"id": catalogue_id})
 