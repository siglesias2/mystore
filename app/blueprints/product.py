"""product module"""
from hashlib import new
import json
from re import S
from flask import Blueprint, request
from datetime import datetime
from models.product_model import Product
from schemas.product_schema import ProductSchema
from response.generic_response import Responses

product_bp = Blueprint('product', __name__,url_prefix="/products")

@product_bp.get("")
def index():
    """Get all product that are not deleted"""
    params = request.args
    product_list = Product().get_all(params)
    products = ProductSchema(many=True).dump(product_list)
    return Responses.index_response(products)
@product_bp.get("/<product_id>")
def show(product_id):
    """get an product by the given id"""
    product = Product().get_one_by({"id": product_id})
    product = ProductSchema(many=False).dump(product)
    if product:
        return Responses.show_response(product)
    return Responses.not_found_response(product)
@product_bp.post("")
def create():
     """create a new product"""       
     params = json.loads(request.data)
     product = Product(**params)
     product.create()
     new_supp = product.get_one_by(params)
     return Responses.create_response(ProductSchema(many=False).dump(new_supp))
@product_bp.put("/<product_id>")
def update(product_id):
    """Update the product according to the given id"""
    body = json.loads(request.data)
    updated_emp = product().update(product_id, body)
    if updated_emp:
        product = ProductSchema(many=False).dump(updated_emp)
        return Responses.update_response(product)
    return Responses.not_found_response({"id": product_id})

@product_bp.delete("/<product_id>")
def logic_delete(product_id):
    """Make a logical delete of an product"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    product = Product().update(product_id, {"deleted_at": deleted_at})
    if product:
        product = ProductSchema(many=False).dump(product)
        return Responses.logical_delete(product)
    return Responses.not_found_response({"id": product_id})

@product_bp.delete("/<product_id>/destroy")
def destroy(product_id):
    """Make a permanent delete of an product"""
    response = Product().destroy(product_id)
    if response:
        return Responses.destroy_resource()
    return Responses.not_found_response({"id": product_id})
 