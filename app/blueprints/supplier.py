"""supplier module"""
from hashlib import new
import json
from re import S
from flask import Blueprint, request
from datetime import datetime
from models.Supplier_model import Supplier
from schemas.supplier_schemas import SupplierSchema
from response.generic_response import Responses

supplier_bp = Blueprint('supplier', __name__,url_prefix="/suppliers")

@supplier_bp.get("")
def index():
    """Get all supplier that are not deleted"""
    params = request.args
    supplier_list = Supplier().get_all(params)
    suppliers = SupplierSchema(many=True).dump(supplier_list)
    return Responses.index_response(suppliers)
@supplier_bp.get("/<supplier_id>")
def show(supplier_id):
    """get an supplier by the given id"""
    supplier = Supplier().get_one_by({"id": supplier_id})
    supplier = SupplierSchema(many=False).dump(supplier)
    if supplier:
        return Responses.show_response(supplier)
    return Responses.not_found_response(supplier)
@supplier_bp.post("")
def create():
     """create a new supplier"""       
     params = json.loads(request.data)
     supplier = Supplier(**params)
     supplier.create()
     new_supp = supplier.get_one_by(params)
     return Responses.create_response(SupplierSchema(many=False).dump(new_supp))
@supplier_bp.put("/<supplier_id>")
def update(supplier_id):
    """Update the supplier according to the given id"""
    body = json.loads(request.data)
    updated_emp = Supplier().update(supplier_id, body)
    if updated_emp:
        supplier = SupplierSchema(many=False).dump(updated_emp)
        return Responses.update_response(supplier)
    return Responses.not_found_response({"id": supplier_id})

@supplier_bp.delete("/<supplier_id>")
def logic_delete(supplier_id):
    """Make a logical delete of an supplier"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    supplier = Supplier().update(supplier_id, {"deleted_at": deleted_at})
    if supplier:
        supplier = SupplierSchema(many=False).dump(supplier)
        return Responses.logical_delete(supplier)
    return Responses.not_found_response({"id": supplier_id})

@supplier_bp.delete("/<supplier_id>/destroy")
def destroy(supplier_id):
    """Make a permanent delete of an supplier"""
    response = Supplier().destroy(supplier_id)
    if response:
        return Responses.destroy_resource()
    return Responses.not_found_response({"id": supplier_id})
 