"""product order detail module"""
import json
from flask import Blueprint, request
from datetime import datetime
from models.order_detail_model import OrderDetail
from schemas.order_detail_schemas import OrderDetailSchema
from models.product_order_detail_model import ProductOrderDetail
from schemas.product_order_detail_schemas import ProductOrderDetailSchema
from response.generic_response import Responses

product_order_detail_bp = Blueprint('product_order_detail', __name__,url_prefix="/product_order_details")

@product_order_detail_bp.get("")
def index():
    """Get all product order detail that are not deleted"""
    params = request.args
    product_order_detail_list = ProductOrderDetail().get_all(params)
    product_order_details = ProductOrderDetailSchema(many=True).dump(product_order_detail_list)
    return Responses.index_response(product_order_details)
@product_order_detail_bp.get("/<product_order_detail_id>")
def show(product_order_detail_id):
    """get an product order detail by the given id"""
    product_order_detail = ProductOrderDetail().get_one_by({"order_detail_id": product_order_detail_id})
    product_order_detail = ProductOrderDetailSchema(many=False).dump(product_order_detail)
    if product_order_detail:
        return Responses.show_response(product_order_detail)
    return Responses.not_found_response(product_order_detail)

@product_order_detail_bp.post("")
def create():
     """create a new product order detail"""       
     params = json.loads(request.data)
     product_order_detail = ProductOrderDetail(**params)
     product_order_detail.create()
     new_supp = product_order_detail.get_one_by(params)
     return Responses.create_response(ProductOrderDetailSchema(many=False).dump(new_supp))
 
@product_order_detail_bp.put("/<order_detail_id>")
def update(product_order_detail_id):
    """Update the order_detail according to the given id"""
    body = json.loads(request.data)
    updated_prod_ord = OrderDetail().update(product_order_detail_id, body)
    if updated_prod_ord:
        order_detail = OrderDetailSchema(many=False).dump(updated_prod_ord)
        return Responses.update_response(order_detail)
    return Responses.not_found_response({"id": product_order_detail_id})

@product_order_detail_bp.delete("/<order_detail_id>")
def logic_delete(product_order_detail_id):
    """Make a logical delete of an order_detail"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    product_order_detail = OrderDetail().update(product_order_detail_id, {"deleted_at": deleted_at})
    if product_order_detail:
        product_order_detail = OrderDetailSchema(many=False).dump(product_order_detail)
        return Responses.logical_delete(product_order_detail)
    return Responses.not_found_response({"id": product_order_detail_id})

@product_order_detail_bp.delete("/<order_detail_id>/destroy")
def destroy(product_order_detail_id):
    """Make a permanent delete of an order_detail"""
    response = ProductOrderDetail().destroy(product_order_detail_id)
    if response:
        return Responses.destroy_resource()
    return Responses.not_found_response({"id": product_order_detail_id})
 