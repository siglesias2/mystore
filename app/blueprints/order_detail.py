"""order detail module"""
from hashlib import new
import json
from re import S
from flask import Blueprint, request
from datetime import datetime
from models.order_detail_model import OrderDetail
from schemas.order_detail_schemas import OrderDetailSchema
from response.generic_response import Responses

order_detail_bp = Blueprint('order_detail', __name__,url_prefix="/order_details")

@order_detail_bp.get("")
def index():
    """Get all order detail that are not deleted"""
    params = request.args
    order_detail_list = OrderDetail().get_all(params)
    order_details = OrderDetailSchema(many=True).dump(order_detail_list)
    return Responses.index_response(order_details)
@order_detail_bp.get("/<order_detail_id>")
def show(order_detail_id):
    """get an order details by the given id"""
    order_detail = order_detail().get_one_by({"id": order_detail_id})
    order_detail = OrderDetailSchema(many=False).dump(order_detail)
    if order_detail:
        return Responses.show_response(order_detail)
    return Responses.not_found_response(order_detail)
@order_detail_bp.post("")
def create():
     """create a new order detail"""       
     params = json.loads(request.data)
     order_detail = OrderDetail(**params)
     order_detail.create()
     new_supp = order_detail.get_one_by(params)
     return Responses.create_response(OrderDetailSchema(many=False).dump(new_supp))
 
@order_detail_bp.put("/<order_detail_id>")
def update(order_detail_id):
    """Update the order_detail according to the given id"""
    body = json.loads(request.data)
    updated_emp = OrderDetail().update(order_detail_id, body)
    if updated_emp:
        order_detail = OrderDetailSchema(many=False).dump(updated_emp)
        return Responses.update_response(order_detail)
    return Responses.not_found_response({"id": order_detail_id})

@order_detail_bp.delete("/<order_detail_id>")
def logic_delete(order_detail_id):
    """Make a logical delete of an order_detail"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    order_detail = OrderDetail().update(order_detail_id, {"deleted_at": deleted_at})
    if order_detail:
        order_detail = OrderDetailSchema(many=False).dump(order_detail)
        return Responses.logical_delete(order_detail)
    return Responses.not_found_response({"id": order_detail_id})

@order_detail_bp.delete("/<order_detail_id>/destroy")
def destroy(order_detail_id):
    """Make a permanent delete of an order_detail"""
    response = OrderDetail().destroy(order_detail_id)
    if response:
        return Responses.destroy_resource()
    return Responses.not_found_response({"id": order_detail_id})
 
 