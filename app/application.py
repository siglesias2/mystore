""" Main flask module"""
from flask import Flask
from db import db 
from config import Config
from blueprints.supplier import supplier_bp
from blueprints.product import product_bp
from blueprints.order_detail import order_detail_bp
from blueprints.catalogue import catalogue_bp
from blueprints.product_order_detail import product_order_detail_bp


app = Flask(__name__)
app.config.from_object(Config())

app.register_blueprint(supplier_bp)
app.register_blueprint(product_bp)
app.register_blueprint(order_detail_bp)
app.register_blueprint(catalogue_bp)
app.register_blueprint(product_order_detail_bp)

db.init_app(app)

#@app.route('/')
#def index():
#    """Modulo application"""
#    return 'Hola Mundo0o!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
