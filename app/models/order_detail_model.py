"""model for order details"""
from db import db 

class OrderDetail(db.Model):
    """order detail model class"""
    __tablename__  = 'order_detail'
    id = db.Column(db.Integer, primary_key= True)
    date_delivery = db.Column(db.Date, nullable=False)
    order_shipped = db.Column(db.Integer, nullable=True)
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'),nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    create_by = db.Column(db.DateTime, nullable=True)
    update_by = db.Column(db.DateTime, nullable=True)
    products_order_detail = db.relationship('ProductOrderDetail', backref='order_detail')

    def get_all(self, params=None):
        """get all order detail that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def get_one_by(self, params):
        """get the first resource by given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """create a new order detail in DB"""
        db.session.add(self)
        db.session.commit()
        
    def update(self, ord_id, params):
        """Update theorder_detail data in DB"""
        order_detail = self.get_one_by({"id": ord_id, "deleted_at": None})
        if order_detail:
            for param, value in params.items():
                setattr(order_detail, param, value)
            db.session.commit()
            return self.get_one_by({"id": ord_id})
        return None

    def destroy(self, ord_id):
        """Destroy order detail in  DB"""
        order_detail = self.get_one_by({"id": ord_id})
        if order_detail:
            db.session.delete(order_detail)
            db.session.commit()
            return True
        return False
    