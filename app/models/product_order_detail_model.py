"""model for order details"""
from db import db 

class ProductOrderDetail(db.Model):
    """order detail model class"""
    __tablename__  = 'product_order_detail'
    order_detail_id = db.Column(db.Integer, db.ForeignKey('order_detail.id'), primary_key= True, nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'),nullable=False)
    quantity = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    create_by = db.Column(db.DateTime, nullable=True)
    update_by = db.Column(db.DateTime, nullable=True)

    def get_all(self, params=None):
        """get all product order details that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def get_one_by(self, params):
        """get the first resource by given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """create a new product order detail in DB"""
        db.session.add(self)
        db.session.commit()
    