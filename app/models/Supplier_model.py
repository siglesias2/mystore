"""model for supplier"""
from db import db

class Supplier(db.Model):
    """Supplier model class"""
    __tablename__  = 'supplier'
    id = db.Column(db.Integer, primary_key= True)
    name = db.Column(db.String(30), nullable=False)
    location = db.Column(db.String(30), nullable=True)
    representative_email = db.Column(db.String(30), nullable=False)
    representative_phone = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    create_by = db.Column(db.DateTime, nullable=True)
    update_by = db.Column(db.DateTime, nullable=True)
    order_detail = db.relationship('OrderDetail', backref='supplier')
    
    def get_all(self, params=None):
        """get all employees that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def get_one_by(self, params):
        """get the first resource by given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """create a new supplier in DB"""
        db.session.add(self)
        db.session.commit()
        
    def update(self, supp_id, params):
        """Update the supplier data in DB"""
        supplier = self.get_one_by({"id": supp_id, "deleted_at": None})
        if supplier:
            for param, value in params.items():
                setattr(supplier, param, value)
            db.session.commit()
            return self.get_one_by({"id": supp_id})
        return None

    def destroy(self, supp_id):
        """Destroy an supplier in  DB"""
        supplier = self.get_one_by({"id": supp_id})
        if supplier:
            db.session.delete(supplier)
            db.session.commit()
            return True
        return False
    