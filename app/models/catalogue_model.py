"""model for catalogue"""
import imp
from itertools import product
from unicodedata import category
from db import db
from models.product_model import Product

class Catalogue(db.Model):
    """Supplier model class"""
    __tablename__  = 'catalogue'
    id = db.Column(db.Integer, primary_key= True)
    category = db.Column(db.String(30), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    create_by = db.Column(db.DateTime, nullable=True)
    update_by = db.Column(db.DateTime, nullable=True)
    Products = db.relationship('Product', backref='catalogue')
    
    def get_all(self, params=None):
        """get all catalogues that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def get_one_by(self, params):
        """get the first resource by given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """create a new supplier in DB"""
        db.session.add(self)
        db.session.commit()
        
    def update(self, cat_id, params):
        """Update the catalogue data in DB"""
        catalogue = self.get_one_by({"id": cat_id, "deleted_at": None})
        if catalogue:
            for param, value in params.items():
                setattr(catalogue, param, value)
            db.session.commit()
            return self.get_one_by({"id": cat_id})
        return None

    def destroy(self, cat_id):
        """Destroy an catalogue in  DB"""
        catalogue = self.get_one_by({"id": cat_id})
        if catalogue:
            db.session.delete(catalogue)
            db.session.commit()
            return True
        return False
    