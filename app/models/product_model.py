"""model for product """
from db import db 
from models.product_order_detail_model import ProductOrderDetail
    
class Product(db.Model):
    """ product model class"""
    __tablename__  = 'product'
    id = db.Column(db.Integer, primary_key= True)
    name = db.Column(db.String(100), nullable=False)
    price_product = db.Column(db.Integer, nullable=True)
    catalogue_id = db.Column(db.Integer, db.ForeignKey('catalogue.id'),nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    create_by = db.Column(db.DateTime, nullable=True)
    update_by = db.Column(db.DateTime, nullable=True)
    products_order_detail = db.relationship('ProductOrderDetail', backref='product')

    def get_all(self, params=None):
        """get all product that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def get_one_by(self, params):
        """get the first resource by given params"""
        return self.query.filter_by(**params).first()
    
    def create(self):
        """create a new product in DB"""
        db.session.add(self)
        db.session.commit()
    
    def update(self, prod_id, params):
        """Update the product data in DB"""
        product = self.get_one_by({"id": prod_id, "deleted_at": None})
        if product:
            for param, value in params.items():
                setattr(product, param, value)
            db.session.commit()
            return self.get_one_by({"id": prod_id})
        return None

    def destroy(self, prod_id):
        """Destroy an product in  DB"""
        product = self.get_one_by({"id": prod_id})
        if product:
            db.session.delete(product)
            db.session.commit()
            return True
        return False