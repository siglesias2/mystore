from ma import ma

class CatalogueSchema(ma.Schema):
    """order schema class"""
    class Meta:
        fields = (
            "id",
            "category"
        )