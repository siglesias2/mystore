from ma import ma

class OrderDetailSchema(ma.Schema):
    """order schema class"""
    class Meta:
        fields = (
            "id",
            "date_delivery",
            "order_shipped",
            "supplier_id"
        )