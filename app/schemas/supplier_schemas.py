"""supplier schemas"""
from ma import ma

class SupplierSchema(ma.Schema):
    """Supplier schema class"""
    class Meta:
        fields = (
            "id",
            "name",
            "location",
            "representative_email",
            "representative_phone"
        )
        