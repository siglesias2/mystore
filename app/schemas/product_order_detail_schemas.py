from ma import ma

class ProductOrderDetailSchema(ma.Schema):
    """order schema class"""
    class Meta:
        fields = (
            "order_detail_id",
            "product_id",
            "quantity",
        )