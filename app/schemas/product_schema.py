from ma import ma

class ProductSchema(ma.Schema):
    """product schema class"""
    class Meta:
        fields = (
            "id",
            "name",
            "price_product",
            "catalogue_id"
        )