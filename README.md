# My store

### caso de uso
La empresa “Mi Tiendita” requiere un sistema que le permita la administración de las
órdenes de compra que realiza con los diferentes proveedores de los productos que
comercializa. La empresa tiene un catálogo de N productos organizados por categorías,
cada producto tiene solo una categoría. La empresa realiza una orden de compra semanal
por proveedor, en ellas aparecen N número de productos diferentes, cada producto aparece
solo una vez con un precio (mayor a 0) y fecha de entrega comprometida con antelación.
Las órdenes de compra son enviadas a los representantes de cada proveedor a su cuenta
de correo. Una vez enviada al proveedor, el cliente no podría agregar o eliminar productos a
una orden de compra.
La empresa solicita los siguientes reportes:
* lista de productos vendidos por proveedor.
* lista de precios por proveedor de un producto en particular. 
